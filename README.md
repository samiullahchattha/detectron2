## Installation

See [INSTALL.md](INSTALL.md).

## Quick Start

Follow the instructions from Installation part to install detectron2 locally. 
Make sure to install flask,werkzeug,flask_cors,opencv

Run ```python app.py```

The server will be up and running on ```0.0.0.0:8080/api/predict```

The server accepts image as input via post request and outputs array
