import os

from flask import Flask, request, jsonify, make_response, send_file
from werkzeug import secure_filename
from flask_cors import CORS, cross_origin
import re
from flask import escape
app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
from deploy import Model
import cv2

model = Model()

@app.route('/')
def hello_world():
    target = os.environ.get('TARGET', 'World')
    return 'Hello {}!\n'.format(target)


@app.route('/api/predict',methods=['POST'])
def predict():    
    if request.method == 'POST':
        input_img = request.files.get('file')
        print(input_img.filename)
        if input_img:
            filename = secure_filename(input_img.filename)
            input_img.save(os.path.join("./UploadedFiles", filename))
            filepath = "./UploadedFiles/"+filename
            read = cv2.imread(filepath)
            output = model.predict(read)
            # return send_file(input_img, mimetype='image/jpeg')
            return jsonify(output)
    else: 
        return jsonify({"unsucessful": True, "error": "Not a post request."})



if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0',port=int(os.environ.get('PORT', 8080)))
    # app.run(debug=True,host='0.0.0.0',port=int(os.environ.get('PORT', 8080)))
    # app.run(debug=True,host='0.0.0.0',port=int(8080))