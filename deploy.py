import torch, torchvision
# torch.__version__

# You may need to restart your runtime prior to this, to let your installation take effect
# Some basic setup
# Setup detectron2 logger
import detectron2
from detectron2.utils.logger import setup_logger
setup_logger()
from flask import jsonify
# import some common libraries
import numpy as np
import cv2
import random 
import json
from PIL import Image
import pytesseract 

from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog

from detectron2.engine import DefaultPredictor



class Model:
    def __init__(self):
        self.cfg = get_cfg()
        # add project-specific config (e.g., TensorMask) here if you're not running a model in detectron2's core library
        self.cfg.merge_from_file("./DLA_mask_rcnn_X_101_32x8d_FPN_3x.yaml")
        self.cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5  # set threshold for this model
        # Find a model from detectron2's model zoo. You can either use the https://dl.fbaipublicfiles.... url, or use the detectron2:// shorthand
        self.cfg.MODEL.WEIGHTS = "./model_final_trimmed.pth"
        self.cfg.MODEL.DEVICE='cpu'
        MetadataCatalog.get(self.cfg.DATASETS.TRAIN[0]).thing_classes = ['text', 'title', 'list', 'table', 'figure']

    def predict(self, inputImage):
        # !wget https://i.ibb.co/gJjCNFd/input.jpg -O input.jpg
        # im = cv2.imread("./input.jpg")
        # im = cv2.imread(inputImage)
        im = inputImage
        # cv2_imshow(im)

        predictor = DefaultPredictor(self.cfg)
        outputs = predictor(im)
        boxes = outputs["instances"].pred_boxes.tensor.cpu().numpy().tolist()
        p_class = outputs["instances"].pred_classes.cpu().numpy().tolist()
        p_scores = outputs["instances"].scores.cpu().numpy().tolist()
        index = 0
        titles = []
        for box in boxes:
            img = im.copy()
            if p_class[index] == 1 and p_scores[index] > 0.9:
                crop_img = img[int(box[1]):int(box[3]), int(box[0]):int(box[2])]
                # cv2_imshow(crop_img)
                im10 = Image.fromarray(crop_img)
                text = pytesseract.image_to_string(im10)
                print(text)
                titles.append(text)
                cv2.waitKey(0)
            index += 1
        return titles 
